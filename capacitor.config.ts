import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'TestIonic7-one',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
