import { Component, ViewChild } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { AppAvailability } from '@awesome-cordova-plugins/app-availability/ngx';
import { AlertController } from '@ionic/angular';
import { Device } from '@capacitor/device';
import { AppLauncher } from '@capacitor/app-launcher';
import { App, AppInfo } from '@capacitor/app';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  standalone: true,
  providers: [AppAvailability],
  imports: [IonicModule, ExploreContainerComponent],
})
export class Tab1Page {
  phoneInfo: any;
  constructor(
    private alertCtrl: AlertController,
    private appAvailability: AppAvailability
  ) {}

  async logDeviceInfo() {
    const info = await Device.getInfo();
    // this.phoneInfo = info;
    console.log(info); // SUCCESS
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Facebook App',
      subHeader: 'Exists?',
      message: 'This is an alert message.',
      buttons: ['OK'],
    });
    await alert.present();
  }

  async checkAppAvailability() {
    console.log('checkAppAvailability');
    /*
    let schema: string;
      if (this.phoneInfo.platform == 'ios') {
        schema = 'fb://';
      } else if (this.phoneInfo.platform == 'android') {
      } else {
        schema = 'fb://';
      }
      schema = 'com.facebook.katana';
    */
    // schema of messenger lite
    let schema = 'com.google.android.apps.meetings';
    this.appAvailability
      .check(schema)
      .then((yes: any) => {
        console.log('app is available');
        this.presentAlert();
      })
      .catch((no: any) => {
        console.log('app is not available');
      });

    let schema2 = 'com.google.android.youtube';
    this.appAvailability
      .check(schema2)
      .then((yes: any) => {
        console.log('Youtube is available');
      })
      .catch((no: any) => {
        console.log('Youtube is not available');
      });

    let schema3 = 'cl.transbank.onepay';
    this.appAvailability.check(schema3).then(
      (success) => {
        console.log({ success });
        window.open('cl.transbank.onepay://', '_system');
      },
      (error) => {
        window.open('cl.transbank.onepay://', '_system');
        console.log({ error });
      }
    );
  }

  async checkAppInstalled() {
    console.log('checkAppInstalled');

    try {
      const { value } = await AppLauncher.canOpenUrl({
        url: 'com.google.android.apps.meetings',
      });
      console.log('isInstalled', value);
    } catch (error) {
      console.log('error', error);
    }

    App.getInfo().then(
      (info: AppInfo) => {
        const res = JSON.stringify(info);
        console.log('App Info', res);
      },
      (error) => {
        console.log('App Info', error);
      }
    );


    try {
      const { value }  = await AppLauncher.canOpenUrl({ url: 'com.google.android.apps.meetings' });
      console.log("🚀 ~ Tab1Page ~ checkAppInstalled ~ value:", value)

      await AppLauncher.openUrl({ url: 'com.google.android.apps.meetings' });

    } catch (error) {
      console.log("🚀 ~ Tab1Page ~ checkAppInstalled ~ error:", error)
    }
  }
}
