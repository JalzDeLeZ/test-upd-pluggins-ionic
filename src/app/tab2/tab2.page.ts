import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { PhotoService } from '../services/photo.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
  standalone: true,
  imports: [IonicModule, ExploreContainerComponent, CommonModule],
})
export class Tab2Page {
  photos: string[] = [];

  constructor(private PhotoService: PhotoService) {
    this.photos = PhotoService.photos;
  }

  takePhoto() {
    this.PhotoService.addNewToGallery();
  }
}
