import { Component } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
  standalone: true,
  providers: [InAppBrowser],
  imports: [IonicModule, ExploreContainerComponent],
})
export class Tab3Page {
  constructor(
    private iab: InAppBrowser
  ) {}

  inappbrowser() {
    console.log('inappbrowser');
    const url = 'https://ionicframework.com/docs/native/in-app-browser';
    const target = '_system';
    const options = 'location=yes,hidden=yes';
    const browser = this.iab.create(url, target, options);
    browser.show();
  }
}
